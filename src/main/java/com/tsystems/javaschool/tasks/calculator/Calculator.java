package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     * <p>
     * statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     *
     * @return string value containing result of evaluation or null if statement is invalid
     */

    static boolean isOperator(char c) { // возвращяем тру если один из символов ниже
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    static void processOperator(LinkedList<Double> st, char op) {
        Double r = st.removeLast();
        Double l = st.removeLast();
        switch (op) {
            case '+':
                st.add(l + r);
                break;
            case '-':
                st.add(l - r);
                break;
            case '*':
                st.add(l * r);
                break;
            case '/':
                st.add(l / r);
                break;
        }
    }

    public String evaluate(String statement) {
        LinkedList<Double> st = new LinkedList<>();
        LinkedList<Character> op = new LinkedList<>();
        try {
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (c == '(')
                    op.add('(');
                else if (c == ')') {
                    while (op.getLast() != '(')
                        processOperator(st, op.removeLast());
                    op.removeLast();
                } else if (isOperator(c)) {
                    while (!op.isEmpty() && priority(op.getLast()) >= priority(c))
                        processOperator(st, op.removeLast());
                    op.add(c);
                } else {
                    StringBuilder operand = new StringBuilder();
                    while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.'))
                        operand.append(statement.charAt(i++));
                    --i;
                    st.add(Double.parseDouble(operand.toString()));
                }
            }
        while (!op.isEmpty())
            processOperator(st, op.removeLast());
        if ((st.get(0) % 1) == 0 && !(st.get(0).isInfinite())) {
            return Integer.toString(st.get(0).intValue());
        } else if (st.get(0).isInfinite()) {
            return null;
        } else
            return st.get(0).toString();
    }
        catch(Exception e)
    {
        return null;
    }
}
}

