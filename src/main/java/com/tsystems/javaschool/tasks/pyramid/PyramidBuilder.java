package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * to be used in the pyramid
     *  2d array with pyramid inside
     *  {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][]matrix;
        try{
            List<Integer> sorted = inputNumbers.stream().sorted().collect(Collectors.toList());
            int rows =(int)((Math.sqrt(8.0*inputNumbers.size()+1)-1)/2);
            matrix=new int[rows][2*rows-1];
            for (int[] ints : matrix) {
                Arrays.fill(ints, 0);
            }
            int ind=0;
            for (int i = 0; i < rows; i++) {
                int count = i+1;
                for (int j = rows-(count); j < matrix[i].length; j+=2) {
                    if (count>0){
                    matrix[i][j]=sorted.get(ind);
                    ind++;
                    count--;
                    }
                }
            }
            if (!(ind==sorted.size())) throw new CannotBuildPyramidException();
            return matrix;
        }
        catch (Exception e){
            throw new CannotBuildPyramidException();
        }
    }


}
